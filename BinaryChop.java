import java.util.Arrays;

abstract class sort
{
	abstract int SearchElement(int ele, int arr[]);
	abstract int SearchElement(int arr[], int l, int r, int ele);
	abstract int SearchElement(int arr[], int ele);
}
class BinarySearch extends sort
{
	int SearchElement(int ele, int arr[])
	{
		int size = arr.length;
		int mid,l=0, r=size-1;
		while(l<=r)
		{
			mid = l + (r-l) /2;
			if(arr[mid] == ele)
				return mid;
			if(arr[mid] < ele)
				l = mid + 1;
			else
				r = mid - 1;
		}
		return -1;
	}
	
	int SearchElement(int arr[], int l, int r, int ele)
	{
		int mid = l + (r-l) /2;
		if(r >= l)
		{
			if(arr[mid] == ele)
				return mid;
			if(arr[mid] < ele)
				return SearchElement(arr, mid+1, r, ele);
			return SearchElement(arr, l, mid-1, ele);
		}
		return -1;
	}
	
	int SearchElement(int arr[], int ele)
	{
		int size = arr.length;
		//System.out.println(size);
		int mid = size/2;
		if(size > 0)
		{
			if(arr[mid] == ele)
				return mid;
			if(arr[mid] < ele)
			{
				arr = Arrays.copyOfRange(arr, mid+1, size);
				return SearchElement(arr, ele);
			}
			return SearchElement(Arrays.copyOfRange(arr, 0, mid), ele);
		}
		return -1;
	}
}

public class BinaryChop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		sort bs = new BinarySearch();
		int arr[] = {1,2,4,6,10,15,30,45};
		int val = 20;
		System.out.println("Iterative output " + bs.SearchElement(val,arr));
		
		System.out.println("recursive Output " + bs.SearchElement(arr, 0, arr.length-1, val));
	}

}
