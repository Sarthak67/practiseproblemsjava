import java.util.Scanner;
public class SortBalls {
	//selection sort
	static void display(int arr[], int size)
	{
		int i,j;
		for(i=0; i<size-1; i++)
		{
			int min_idx = i;
			for(j=i+1; j< size; j++)
			{
				if(arr[j] < arr[min_idx])
					min_idx = j;
			}
			int tmp = arr[min_idx];
			arr[min_idx] = arr[i];
			arr[i] = tmp;
		}
		for(i=0; i<size; i++)
			System.out.print(arr[i]+" ");
	}
	
	//insertion sort
	static void displayInsertion(int arr[], int val, int size)
	{
		int i=0,j;
		/*while(arr[size]!=0)			//calculated size
			size++;
		*/
		// sorting starts here
		while(arr[i] < val && arr[i] != 0)
			i++;
		while(size > i)
		{
			arr[size] = arr[size-1];
			size--;
		}
		arr[i] = val;
		
		//displaying sorted array
		i=0;
		while(arr[i] != 0)
		{
			System.out.print(arr[i]+ " ");
			i++;
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int A[] = new int[60];
		Scanner sc = new Scanner(System.in);
		int i, j, num;
		i = 0;
		while(true)
		{
			num = sc.nextInt();
			if(num == 0)
				break;
			else
			{
				i++;
				displayInsertion(A, num, i);
			}
		}
	}
}
